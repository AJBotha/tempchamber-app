﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.IO;


namespace Temperature_Chamber_Program
{
    public partial class TempProgram : Form
    {
        public bool GlobalComSet { get; private set; }
        public bool GlobalAmbientSet { get; private set; }

        //User Setpoints
        double finalTempVal = 0;
        double startTempVal = 0;

        double timestep = 0;
        double tempstep = 0;
        double direction = 0;

       public double actualtemp = 25;

        public bool setpointReached = false;

        //For Chamber Control
        public double currentSetpoint = 25;
        public double nextSetpoint;
        public bool waitflag = true;

        public bool upperReached = false;
        public bool lowerReached = false;

        string filepath;

        public TempProgram()
        {
            InitializeComponent();
            CommsBox.DropDownStyle = ComboBoxStyle.DropDownList;

            //See which COM ports are available
            foreach (string portName in System.IO.Ports.SerialPort.GetPortNames())
            {
                serialPort1.PortName = portName;

                
                if (serialPort1.IsOpen == false) // Only list if it is not in use - does not work - .IsOpen is only valid from within this app
                {
                    CommsBox.Items.Add(portName);
                }
            }

            //Load Default values from the spinboxes
            timestep = (int)numericUpDownTimeStep.Value;
            tempstep = (int)numericUpDownTempStep.Value;
            startTempVal = (int)numericUpDownMin.Value;
            finalTempVal = (int)numericUpDown1.Value;
            //Temperature Direction DropDown Box Populate
            ambientDirection.Items.Add("Go to Upper Temp first, then Down");
            ambientDirection.Items.Add("Go to Lower Temp first, then Up");

        }


        private void formFull()
        {
            if (GlobalComSet && GlobalAmbientSet)
            {
                startButton.Enabled = true;
                startButton.BackColor = Color.Green;
                ManualButton.Enabled = true;
            }

        }

        public void createLog()
        {
            string timestamp = DateTime.Now.ToString("yyyyMMddHHmmssfff");
            string logName = "TempChamberLog";
            try
            {

                string folderLoc = "C:\\TemperatureChamberLogs\\";
                System.IO.Directory.CreateDirectory(folderLoc);


                filepath = (folderLoc + logName + timestamp + ".txt");
                using (StreamWriter logfile = File.AppendText(filepath));
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("Error Creating File - run as Admin");
            }

        }

        public void controlChamber()
        {

            if (upperReached == false || lowerReached == false)
            {
                setpointReached = false;

                if (waitflag == false)
                {
                    actualtemp = currentSetpoint; // This is the same as disabling the wait flag
                }
                else
                {
                    actualtemp = readChamber();
                }

                if ((actualtemp >= finalTempVal - 2) && (direction == 0)) // We were going up but we reached the upper limit - start going down
                {
                    direction = 1; // Go Down
                    upperReached = true;
                    nextSetpoint = currentSetpoint - tempstep;
                    sendTemp((int)nextSetpoint);
                    setpointReached = true; // wait minimum timestep
                }
                else if ((actualtemp <= startTempVal + 2) && (direction == 1)) // We were going down but have reached the lower boundry, start going up
                {
                    direction = 0; // Go Up
                    lowerReached = true;
                    nextSetpoint = currentSetpoint + tempstep;
                    sendTemp((int)nextSetpoint);
                    setpointReached = true; // wait minimum timestep
                }


                if ((actualtemp <= finalTempVal) && (direction == 0)) // Continue going up
                {
                    if (actualtemp >= currentSetpoint - 2)
                    {
                        nextSetpoint = currentSetpoint + tempstep;
                        sendTemp((int)nextSetpoint);
                        setpointReached = true; // wait minimum timestep
                    }
                }

                else if ((actualtemp >= startTempVal) && (direction == 1)) // Down Direction
                {
                    if  (actualtemp  <= currentSetpoint + 2)  //-31 is greater than -35
                    {
                        nextSetpoint = currentSetpoint - tempstep;
                        sendTemp((int)nextSetpoint);
                        setpointReached = true; // wait minimum timestep
                    }

                }

                if (setpointReached == true)
                {
                    timer1.Interval = (Convert.ToInt32(timestep) * 1000 *60);
                    currentSetpoint = nextSetpoint;
                }

                else
                {
                    timer1.Interval = (Convert.ToInt32(1000)*30); // 30 Seconds
                    setpointReached = false; // decrease timer value and keep checking if temp has been reached
                }
            }
            else // lower and upper temperature reached
            {
                timer1.Stop();
                reset();
                MessageBox.Show("Program Finished");

            }

        }

        public void sendTemp(int newtemp)
        {
            
            string appendText = DateTime.Now.ToString("HHmmss") + "," + " CurrentTemp:, " 
                +actualtemp.ToString() + "," + " NewSetpointTemp," +newtemp.ToString() + Environment.NewLine;
            File.AppendAllText(filepath, appendText);

            bool negativeTemp = false;
            if (newtemp < 0)
            {
                negativeTemp = true;
                newtemp = newtemp * -1; // Convert to positive number
            }

            int[] stringnumbers = NumbersIn(newtemp);
            int stringlength = stringnumbers.Length;

            upKey();

            if (newtemp ==0)
            {
                zeroKey();
                zeroKey();
            }

            //Navigate to new input temperature
            if (negativeTemp == true)
            {
                //send negative key
               // serialPort1.Write("\r\n - KEY \r\n"); //Debug
                negativeKey();

            }
            else
            {
                //send 0
               // serialPort1.Write("\r\nZero Key \r\n"); //Debug
                zeroKey();

            }

            //navigate to next digit

            //serialPort1.Write("\r\nnewTemp: " + newtemp.ToString());
            for (int i = 0; i < stringlength; i++)
            {

                //serialPort1.Write("\r\nupKey \r\n"); //debug
               // upKey(); // how many of these?
                                               
                int temp = stringnumbers[i];

                //serialPort1.Write(("\r\nDigit: " + temp.ToString()) + "\r\n"); //debug
                if (temp == 0) zeroKey();
                else if (temp == 1) oneKey();
                else if (temp == 2) twoKey();
                else if (temp == 3) threeKey();
                else if (temp == 4) fourKey();
                else if (temp == 5) fiveKey();
                else if (temp == 6) sixKey();
                else if (temp == 7) sevenKey();
                else if (temp == 8) eightKey();
                else if (temp == 9) nineKey();

                
            }
            zeroKey();

            for (int i = 0; i<21; i++)
            {
                upKey();
            }
            //serialPort1.Write("\r\nHandKey/Run /r/n"); //debug
           // handKey();

            

        }


        public int[] NumbersIn(int value) //split numbers into array of single digits
        {
            var numbers = new Stack<int>();
            for (; value > 0; value /= 10)
                numbers.Push(value % 10);
            return numbers.ToArray();
        }

        public double readChamber()
        {
            double actualtemp = currentSetpoint; // change this to read the actual value


            serialPort1.DiscardInBuffer();

            writeESC();

            byte[] val = { 0x33 }; //request data dump
            Task.Delay(500);
            serialPort1.Write(val, 0, 1);
            Task.Delay(500);


            String ReceivedData ="";
            for (int i = 0; i < 8; i++)
            {
                ReceivedData = serialPort1.ReadLine();
            }

            //MessageBox.Show(ReceivedData.ToString());

            if (ReceivedData.Length > 6)
            {

                string[] words = ReceivedData.Split(' ');
               
               
                actualtemp = Convert.ToDouble(words[0]);
               // MessageBox.Show(actualtemp.ToString());
            }

            return actualtemp;
        }


        private void CommsBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            int selectedIndex = CommsBox.SelectedIndex;
            Object selectedItem = CommsBox.SelectedItem;
            serialPort1.PortName = selectedItem.ToString();
            //serialPort1.DataReceived += new SerialDataReceivedEventHandler(DataReceivedHandler); //uart receive
            GlobalComSet = true;
            formFull();
        }


      /*  public static void DataReceivedHandler(
                       object sender,
                       SerialDataReceivedEventArgs e)
        {
            SerialPort sp = (SerialPort)sender;

          
                    string indata = sp.ReadExisting();
            
            //       string indata = sp.ReadLine();


            string[] separatingChars = { "Zx ", " " };
            // String St = "super exemple of string key : text I want to keep - end of my string";

            if (indata.Length >= 40)
                {
                int pFrom = indata.IndexOf("Zx ") + "Zx ".Length;
                int pTo = indata.IndexOf("Zx ") + "Zx ".Length + 4;

                String result = indata.Substring(pFrom, pTo - pFrom);
            }

        }*/




        private void ambientDirection_SelectedIndexChanged(object sender, EventArgs e)
        {

            int selectedIndex = ambientDirection.SelectedIndex;
            Object selectedItem = ambientDirection.SelectedItem;
            //MessageBox.Show("Selected Item Text: " + selectedItem.ToString() + "\n" + "Index: " + selectedIndex.ToString());
            direction = selectedIndex; //1 = down 0 = up
            GlobalAmbientSet = true;
            formFull();
        }

        private void startButton_Click(object sender, EventArgs e)
        {

            startButton.Enabled = false;
            startButton.BackColor = Color.Gray;
            stopButton.BackColor = Color.Red;
            stopButton.Enabled = true;
            timer1.Enabled = true;
            timer1.Interval = (Convert.ToInt32(timestep)*1000*60); // Convert to seconds // TODO convert to minutes
            timer1.Start();

            try //Open the serial Port
            {
             
                if (!serialPort1.IsOpen)
                {
                    serialPort1.Open();
                  //  serialPort1.Write("Serial POrt Opened \r\n");
                }

            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("Unauthorized Access Exception - Failed to Open Serial");
            }

            createLog();
            //Initialize Chamber TODO

            initializeChamber();


        }
//Very Inefficient Serial Functions start here emulating the keypad
        void writeESC()
        {
            byte[] ESC = { 0x1B };
            Task.Delay(300);

            try
            {
                serialPort1.Write(ESC, 0, 1);
            }
            catch (TimeoutException)
            {
                serialPort1.Handshake = Handshake.None;
                MessageBox.Show("Could not Establish Handshake with Temp Chamber - Disabling Handshake");

            }
        }
        void upKey() //ESC + K
        {
            writeESC();
            byte[] upKey = { 0x4B };
            Task.Delay(500);
            serialPort1.Write(upKey, 0, 1);
            
        }
        void negativeKey() //Esc + G
        {
            writeESC();
            byte[] negativeKey = { 0x47 };
            Task.Delay(500);
            serialPort1.Write(negativeKey, 0, 1);
            
        }
        void zeroKey() //Esc + O
        {
            writeESC();
            byte[] zero = { 0x4F };
            Task.Delay(500);
            serialPort1.Write(zero, 0, 1);
        }
        void oneKey()
        {
            writeESC();
            byte[] val = { 0x4E }; //N
            Task.Delay(500);
            serialPort1.Write(val, 0, 1);
        }
        void twoKey()
        {
            writeESC();
            byte[] val = { 0x4A }; //J
            Task.Delay(500);
            serialPort1.Write(val, 0, 1);
        }
        void threeKey()
        {
            writeESC();
            byte[] val = { 0x46 }; //F
            Task.Delay(500);
            serialPort1.Write(val, 0, 1);
        }
        void fourKey()
        {
            writeESC();
            byte[] val = { 0x4D }; //M
            Task.Delay(500);
            serialPort1.Write(val, 0, 1);
        }
        void fiveKey()
        {
            writeESC();
            byte[] val = { 0x49 }; //I
            Task.Delay(500);
            serialPort1.Write(val, 0, 1);
        }
        void sixKey()
        {
            writeESC();
            byte[] val = { 0x45 }; //E
            Task.Delay(500);
            serialPort1.Write(val, 0, 1);
        }
        void sevenKey()
        {
            writeESC();
            byte[] val = { 0x4C }; //L
            Task.Delay(500);
            serialPort1.Write(val, 0, 1);
        }
        void eightKey()
        {
            writeESC();
            byte[] val = { 0x48 }; //H
            Task.Delay(500);
            serialPort1.Write(val, 0, 1);
        }
        void nineKey()
        {
            writeESC();
            byte[] val = { 0x44 }; //D
            Task.Delay(500);
            serialPort1.Write(val, 0, 1);
        }
        void handKey()
        {
            writeESC();
            byte[] val = { 0x43 }; //C
            Task.Delay(500);
            serialPort1.Write(val, 0, 1);
        }
        //Very Inefficient Serial Functions stop here emulating the keypad



        void LF() { // Just to find theeBuggs
            serialPort1.Write("\r\n");
         }


        void initializeChamber()
        { 
            // serialPort1.Write("Initialize Temp Chamber \r\n");

              byte[] Hex1 = { 0x31 };           // ASCII 1
              byte[] Hex2 = { 0x32 };           // ASCII 2
              byte[] DisableHand = { 0x43 };    // ASCII C
              byte[] UpKey = { 0x4B };          // ASCII K

            writeESC();
            Task.Delay(500);
            serialPort1.Write(Hex1, 0, 1);

            Task.Delay(500);
            writeESC();
            Task.Delay(500);
            serialPort1.Write(Hex2, 0, 1);

            Task.Delay(500);
            handKey();
            Task.Delay(500);

            Task.Delay(500);
            upKey();

            readChamber();

            currentSetpoint = Math.Round(actualtemp / 5.0) * 5;

            controlChamber();

        }
        void reset()
        {

            setpointReached = false;

            //For Chamber Control
            currentSetpoint = 25;
            nextSetpoint = currentSetpoint;
            waitflag = false;

            upperReached = false;
            lowerReached = false;

            filepath = "";

        }


        private void stopButton_Click(object sender, EventArgs e)
        {
            startButton.Enabled = true;
            stopButton.BackColor = Color.Gray;
            startButton.BackColor = Color.Green;
            timer1.Stop();

            //STOP
            Task.Delay(500);
            handKey();
            Task.Delay(500);
           
            //Return the chamber to manual input Mode
            byte[] Hex1 = { 0x31 };           // ASCII 1
            writeESC();

            Task.Delay(500);
            serialPort1.Write(Hex1, 0, 1);
            Task.Delay(500);

            writeESC();

            Task.Delay(500);
            serialPort1.Write(Hex1, 0, 1);


            if (serialPort1.IsOpen)
            {
                serialPort1.Close();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            
            controlChamber();
           // MessageBox.Show("Timer Ticked");
        }

        private void numericUpDownMin_ValueChanged(object sender, EventArgs e)
        {
             startTempVal = (int)numericUpDownMin.Value;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e) //Upper Temp Value
        {
            finalTempVal = (int)numericUpDown1.Value;
        }

        private void numericUpDownTempStep_ValueChanged(object sender, EventArgs e)
        {
            tempstep = (int)numericUpDownTempStep.Value;
        }

        private void numericUpDownTimeStep_ValueChanged(object sender, EventArgs e)
        {
            timestep = (int)numericUpDownTimeStep.Value;
            //timestep = (timestep * 60);
        }

        private void actualTempCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (actualTempCheckBox.Checked)
            { 
               waitflag = true; // Ignore Timestep if the setpoint has not been reached
            }
            else
            {
                waitflag = false;
            }
        }

        private void TestButton_Click(object sender, EventArgs e)
        {

            try //Open the serial Port
            {

                if (!serialPort1.IsOpen)
                {
                    serialPort1.Open();
                    //  serialPort1.Write("Serial POrt Opened \r\n");
                }

            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("Unauthorized Access Exception - Failed to Open Serial");
            }

            byte[] Hex1 = { 0x31 };           // ASCII 1

            writeESC();
            Task.Delay(500);
            serialPort1.Write(Hex1, 0, 1);
        }
    }
}
