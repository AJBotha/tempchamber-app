﻿namespace Temperature_Chamber_Program
{
    partial class TempProgram
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.startTempLabel = new System.Windows.Forms.Label();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.CommsBox = new System.Windows.Forms.ComboBox();
            this.selectComLbl = new System.Windows.Forms.Label();
            this.finalTempLbl = new System.Windows.Forms.Label();
            this.lbl1 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tempIncrements = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.actualTempCheckBox = new System.Windows.Forms.CheckBox();
            this.ambientDirection = new System.Windows.Forms.ComboBox();
            this.ambientLbl = new System.Windows.Forms.Label();
            this.startButton = new System.Windows.Forms.Button();
            this.stopButton = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.numericUpDownMin = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownTempStep = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownTimeStep = new System.Windows.Forms.NumericUpDown();
            this.ManualButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTempStep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTimeStep)).BeginInit();
            this.SuspendLayout();
            // 
            // startTempLabel
            // 
            this.startTempLabel.AutoSize = true;
            this.startTempLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startTempLabel.Location = new System.Drawing.Point(12, 61);
            this.startTempLabel.Name = "startTempLabel";
            this.startTempLabel.Size = new System.Drawing.Size(106, 34);
            this.startTempLabel.TabIndex = 1;
            this.startTempLabel.Text = "Select Lower \r\nTemperature";
            // 
            // serialPort1
            // 
            this.serialPort1.Handshake = System.IO.Ports.Handshake.RequestToSend;
            this.serialPort1.ReadTimeout = 6000;
            this.serialPort1.RtsEnable = true;
            this.serialPort1.WriteTimeout = 6000;
            // 
            // CommsBox
            // 
            this.CommsBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CommsBox.FormattingEnabled = true;
            this.CommsBox.Location = new System.Drawing.Point(21, 34);
            this.CommsBox.Name = "CommsBox";
            this.CommsBox.Size = new System.Drawing.Size(96, 21);
            this.CommsBox.TabIndex = 2;
            this.CommsBox.SelectedIndexChanged += new System.EventHandler(this.CommsBox_SelectedIndexChanged);
            // 
            // selectComLbl
            // 
            this.selectComLbl.AutoSize = true;
            this.selectComLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectComLbl.Location = new System.Drawing.Point(12, 14);
            this.selectComLbl.Name = "selectComLbl";
            this.selectComLbl.Size = new System.Drawing.Size(127, 17);
            this.selectComLbl.TabIndex = 3;
            this.selectComLbl.Text = "Select COM Port";
            // 
            // finalTempLbl
            // 
            this.finalTempLbl.AutoSize = true;
            this.finalTempLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.finalTempLbl.Location = new System.Drawing.Point(12, 128);
            this.finalTempLbl.Name = "finalTempLbl";
            this.finalTempLbl.Size = new System.Drawing.Size(107, 34);
            this.finalTempLbl.TabIndex = 4;
            this.finalTempLbl.Text = "Select Upper \r\nTemperature";
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl1.Location = new System.Drawing.Point(80, 95);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(25, 17);
            this.lbl1.TabIndex = 6;
            this.lbl1.Text = "°C";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(80, 166);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 17);
            this.label1.TabIndex = 7;
            this.label1.Text = "°C";
            // 
            // tempIncrements
            // 
            this.tempIncrements.AutoSize = true;
            this.tempIncrements.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tempIncrements.Location = new System.Drawing.Point(12, 200);
            this.tempIncrements.Name = "tempIncrements";
            this.tempIncrements.Size = new System.Drawing.Size(190, 34);
            this.tempIncrements.TabIndex = 8;
            this.tempIncrements.Text = "Temperature Increments \r\n(Stepsize)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(80, 247);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 17);
            this.label2.TabIndex = 10;
            this.label2.Text = "°C";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 279);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 17);
            this.label3.TabIndex = 11;
            this.label3.Text = "Time Step";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(80, 300);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 17);
            this.label4.TabIndex = 13;
            this.label4.Text = "Minutes";
            // 
            // actualTempCheckBox
            // 
            this.actualTempCheckBox.AutoSize = true;
            this.actualTempCheckBox.Checked = true;
            this.actualTempCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.actualTempCheckBox.Location = new System.Drawing.Point(24, 347);
            this.actualTempCheckBox.Name = "actualTempCheckBox";
            this.actualTempCheckBox.Size = new System.Drawing.Size(335, 30);
            this.actualTempCheckBox.TabIndex = 14;
            this.actualTempCheckBox.Text = "Wait for Actual Temp to reach Setpoint before resuming program?\r\n(If the timestep" +
    " is chosen too small this will delay the next setpoint)";
            this.actualTempCheckBox.UseVisualStyleBackColor = true;
            this.actualTempCheckBox.CheckedChanged += new System.EventHandler(this.actualTempCheckBox_CheckedChanged);
            // 
            // ambientDirection
            // 
            this.ambientDirection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ambientDirection.FormattingEnabled = true;
            this.ambientDirection.Location = new System.Drawing.Point(174, 35);
            this.ambientDirection.Name = "ambientDirection";
            this.ambientDirection.Size = new System.Drawing.Size(209, 21);
            this.ambientDirection.TabIndex = 15;
            this.ambientDirection.SelectedIndexChanged += new System.EventHandler(this.ambientDirection_SelectedIndexChanged);
            // 
            // ambientLbl
            // 
            this.ambientLbl.AutoSize = true;
            this.ambientLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ambientLbl.Location = new System.Drawing.Point(171, 14);
            this.ambientLbl.Name = "ambientLbl";
            this.ambientLbl.Size = new System.Drawing.Size(112, 17);
            this.ambientLbl.TabIndex = 16;
            this.ambientLbl.Text = "From Ambient:";
            // 
            // startButton
            // 
            this.startButton.BackColor = System.Drawing.SystemColors.Control;
            this.startButton.Enabled = false;
            this.startButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startButton.Location = new System.Drawing.Point(83, 423);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(69, 42);
            this.startButton.TabIndex = 17;
            this.startButton.Text = "Start";
            this.startButton.UseVisualStyleBackColor = false;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // stopButton
            // 
            this.stopButton.Enabled = false;
            this.stopButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stopButton.Location = new System.Drawing.Point(257, 423);
            this.stopButton.Name = "stopButton";
            this.stopButton.Size = new System.Drawing.Size(68, 42);
            this.stopButton.TabIndex = 18;
            this.stopButton.Text = "Stop";
            this.stopButton.UseVisualStyleBackColor = true;
            this.stopButton.Click += new System.EventHandler(this.stopButton_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // numericUpDownMin
            // 
            this.numericUpDownMin.Location = new System.Drawing.Point(21, 100);
            this.numericUpDownMin.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.numericUpDownMin.Minimum = new decimal(new int[] {
            35,
            0,
            0,
            -2147483648});
            this.numericUpDownMin.Name = "numericUpDownMin";
            this.numericUpDownMin.Size = new System.Drawing.Size(53, 20);
            this.numericUpDownMin.TabIndex = 19;
            this.numericUpDownMin.Value = new decimal(new int[] {
            30,
            0,
            0,
            -2147483648});
            this.numericUpDownMin.ValueChanged += new System.EventHandler(this.numericUpDownMin_ValueChanged);
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(21, 166);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            20,
            0,
            0,
            -2147483648});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(53, 20);
            this.numericUpDown1.TabIndex = 20;
            this.numericUpDown1.Value = new decimal(new int[] {
            70,
            0,
            0,
            0});
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // numericUpDownTempStep
            // 
            this.numericUpDownTempStep.Location = new System.Drawing.Point(21, 247);
            this.numericUpDownTempStep.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numericUpDownTempStep.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownTempStep.Name = "numericUpDownTempStep";
            this.numericUpDownTempStep.Size = new System.Drawing.Size(53, 20);
            this.numericUpDownTempStep.TabIndex = 21;
            this.numericUpDownTempStep.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDownTempStep.ValueChanged += new System.EventHandler(this.numericUpDownTempStep_ValueChanged);
            // 
            // numericUpDownTimeStep
            // 
            this.numericUpDownTimeStep.Location = new System.Drawing.Point(21, 300);
            this.numericUpDownTimeStep.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.numericUpDownTimeStep.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownTimeStep.Name = "numericUpDownTimeStep";
            this.numericUpDownTimeStep.Size = new System.Drawing.Size(53, 20);
            this.numericUpDownTimeStep.TabIndex = 22;
            this.numericUpDownTimeStep.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDownTimeStep.ValueChanged += new System.EventHandler(this.numericUpDownTimeStep_ValueChanged);
            // 
            // ManualButton
            // 
            this.ManualButton.Enabled = false;
            this.ManualButton.Location = new System.Drawing.Point(308, 111);
            this.ManualButton.Name = "ManualButton";
            this.ManualButton.Size = new System.Drawing.Size(75, 23);
            this.ManualButton.TabIndex = 23;
            this.ManualButton.Text = "Manual";
            this.ManualButton.UseVisualStyleBackColor = true;
            this.ManualButton.Click += new System.EventHandler(this.TestButton_Click);
            // 
            // TempProgram
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(407, 488);
            this.Controls.Add(this.ManualButton);
            this.Controls.Add(this.numericUpDownTimeStep);
            this.Controls.Add(this.numericUpDownTempStep);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.numericUpDownMin);
            this.Controls.Add(this.stopButton);
            this.Controls.Add(this.startButton);
            this.Controls.Add(this.ambientLbl);
            this.Controls.Add(this.ambientDirection);
            this.Controls.Add(this.actualTempCheckBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tempIncrements);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbl1);
            this.Controls.Add(this.finalTempLbl);
            this.Controls.Add(this.selectComLbl);
            this.Controls.Add(this.CommsBox);
            this.Controls.Add(this.startTempLabel);
            this.Name = "TempProgram";
            this.Text = "Temp Chamber Program";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTempStep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTimeStep)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label startTempLabel;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.ComboBox CommsBox;
        private System.Windows.Forms.Label selectComLbl;
        private System.Windows.Forms.Label finalTempLbl;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label tempIncrements;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox actualTempCheckBox;
        private System.Windows.Forms.ComboBox ambientDirection;
        private System.Windows.Forms.Label ambientLbl;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.Button stopButton;
        public System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.NumericUpDown numericUpDownMin;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.NumericUpDown numericUpDownTempStep;
        private System.Windows.Forms.NumericUpDown numericUpDownTimeStep;
        private System.Windows.Forms.Button ManualButton;
    }
}

