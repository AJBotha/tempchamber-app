 switch (ArgT144TestType)
			{
				case 1:								// Set Temperature Point 
				{
					OpenComConfig (ArgT144Port, "",9600, 0, 8, 1, 1024, 512); //OpenComConfig (int COMPort, char deviceName[], long baudRate, int parity, int dataBits, int stopBits, int inputQueueSize, int outputQueueSize); 
					Delay(0.2);
					
					if (ArgT144TempPoint1 + ArgT144TempPoint2 == 0x93)
					{
						ComWrtByte(ArgT144Port, 0x1B);					 // Enrty Keyboard "ESC1"
						Delay(0.2);
						ComWrtByte(ArgT144Port, 0x01);					 // 
						Delay(0.2);
						ComWrtByte(ArgT144Port, 0x1B);
						Delay(0.5);
						ComWrtByte(ArgT144Port, 0x02);					 // Enrty Serial Interface "ESC2"  
						Delay(0.2);
						ComWrtByte(ArgT144Port, 0x1B);
						Delay(0.2);
						ComWrtByte(ArgT144Port, 0x43);					 // Disable Hand Operation "ESC2" 
						Delay(0.2);
						ComWrtByte(ArgT144Port, 0x1B);
						Delay(0.2);
						ComWrtByte(ArgT144Port, 0x4B);					 // Up Key "ESCK" 
						Delay(0.2);
						ComWrtByte(ArgT144Port, 0x1B);
						Delay(0.5);
						ComWrtByte(ArgT144Port, 0x4B);					 // Up Key "ESCK" 
						Delay(0.2);
					}
					if (ArgT144TempPoint1 + ArgT144TempPoint2 != 0x93)
					{     
						Delay(0.2);
						ComWrtByte(ArgT144Port, 0x4B);					 // Up Key "ESCK" 
						Delay(0.2);
						ComWrtByte(ArgT144Port, 0x1B);
						Delay(0.5);
						ComWrtByte(ArgT144Port, 0x4B);					 // Up Key "ESCK" 
						Delay(0.2);
						ComWrtByte(ArgT144Port, 0x1B);
						Delay(0.2);
						ComWrtByte(ArgT144Port, 0x4B);					// Up Key "ESCK"   
						Delay(0.2);
					}
					ComWrtByte(ArgT144Port, 0x1B);
					Delay(0.2);
					ComWrtByte(ArgT144Port,ArgT144Sign);
					Delay(0.2);
					ComWrtByte(ArgT144Port, 0x1B);
					Delay(0.2);
					ComWrtByte(ArgT144Port,ArgT144TempPoint1);
					Delay(0.2);
					ComWrtByte(ArgT144Port, 0x1B);
					Delay(0.2);
					ComWrtByte(ArgT144Port,ArgT144TempPoint2);
					Delay(0.2);
					ComWrtByte(ArgT144Port, 0x1B);
					Delay(0.2);
					ComWrtByte(ArgT144Port, 0x4F);
					Delay(0.2);
					for (j == 0; j<20; j++)
					{
						ComWrtByte(ArgT144Port, 0x1B);
					  	Delay(0.2);
						ComWrtByte(ArgT144Port, 0x4B);					 // Up Key "ESCK" 
						Delay(0.2);
						Res.TesT144RxData[j] = ComRdByte(ArgT144Port); 
					}
					//	ArgT144SubString=ArgT144TempPoint;
					while (!1)
					{
						RxNo =400;
						ComRd (ArgT144Port, RxBuff, RxNo);  	//Reads RxNo of bytes from the input queue of the selected COM port and stores them in RxBuff
						memset (Res.TesT144RxData, 0, 255);  
						strncpy(Res.TesT144RxData, &RxBuff[0], 2);
						sprintf(MiscStr,"Transmitting [%s] to RS232", ArgT144TxData);
		  	
			  			sprintf(&Res.TesT144Rtn[0], "%s", ArgT144SubString); 
			  			if (strstr(&Res.TesT144RxData[0],ArgT144SubString))
			  			{
			  				memset (Res.TesT144RxData, 0, 255); 	
			  				sprintf(&Res.TesT144RxData[0], "%s", ArgT144SubString);  //If sub-string found then set RxData to sub-string to be verified as correct
		  				}
			  		}
			  	}
				break;
		 	}   